import makeGenerator from "random-seed";
import randomstring from "randomstring";
import { redirect } from "@sveltejs/kit";

import { getLogger } from "$lib/services/logger.js";
import {
  getDB,
  getNeocitiesSitesTotal,
  getNeocitiesSiteByID,
} from "$lib/services/db";

let CACHED_NEOCITIES_SITES_TOTAL = null;
const DEFAULT_RANDOM_STRING_LEGNTH = 6;

export async function load(request) {
  const logger = await getLogger();
  const db = await getDB({ logger });

  // Get the number of total sites if not currently cached
  if (CACHED_NEOCITIES_SITES_TOTAL === null || CACHED_NEOCITIES_SITES_TOTAL === 0) {
    CACHED_NEOCITIES_SITES_TOTAL = await getNeocitiesSitesTotal({ db, logger });
  }

  // Determine the seed
  const seed = request.url.searchParams.get("seed") ?? randomstring.generate(DEFAULT_RANDOM_STRING_LEGNTH);

  // Generate the random numeric ID that will represent the  we should get
  const idGenerator = makeGenerator(seed);
  const id = idGenerator.range(CACHED_NEOCITIES_SITES_TOTAL) + 1;

  // Look up the correct neocities
  let neocity;
  try {
    neocity = await getNeocitiesSiteByID({ id, db, logger });
    if (!neocity) { throw new Error("Failed to load neocity"); }
  } catch(err: any) {
    // TODO: Report error to sentry
    // request.extra?.sentry?.report();

    const title = `Failed to load Neocity with ID [${id}]`;
    const subtitle = "Sorry, we couldn't load your neocity. An unexpected error occurred.";
    throw redirect(303, `/error?title=${title}&subtitle=${subtitle}`);
  }

  return {
    seed,
    id,
    neocity: neocity.toPOJO(),
  };
}
