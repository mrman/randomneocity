import { redirect } from "@sveltejs/kit";

// GET /new
export async function GET() {
  throw redirect(303, "/");
}
