// RSS runs on 3000, App runs on 30001
export const DEFAULT_PORT = 3001;
export const DEFAULT_HOST = "127.0.0.1";

// User facing
export const DEFAULT_SUPPORT_EMAIL = "vados+randomneocities@vadosware.io";
export const DEFAULT_HELLO_EMAIL = "vados+randomneocities@vadosware.io";

// Login
export const DEFAULT_LOGIN_COOKIE_MAX_AGE_SECONDS = 60 * 60 * 24 * 30; // 30 days
export const DEFAULT_LOGIN_COOKIE_SECURE = "true";
export const DEFAULT_WFL_USER_AUTH_COOKIE_NAME = "waaard-wfl-user";

// Email
export const DEFAULT_MAILER_SMTP_HOST = "localhost";
export const DEFAULT_MAILER_SMTP_PORT = 465;
export const DEFAULT_MAILER_FROM_ADDRESS = "noreply@staging.randomneo.city";
