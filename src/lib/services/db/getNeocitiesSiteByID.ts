import { Logger } from "pino";
import { DataSource } from "typeorm";

import { NeocitiesSite } from "$lib/services/db/models";

interface GetNeocitiesSiteByID {
  db: DataSource;
  logger?: Logger;

  id: number;
}

/**
 * Get a single Neocities site by ID
 *
 * @param {GetNeocitiesSiteByID} args
 * @param {DataSource} args.db
 * @param {Logger} args.logger
 * @returns {Promise<number>} A Promise that resolves to the number of geocities
 */
export async function getNeocitiesSiteByID(args: GetNeocitiesSiteByID): Promise<NeocitiesSite> {
  const { db, logger, id } = args;

  if (!id) { 
    logger?.error({ data: { id }}, "missing/invalid Neocities site ID");
    throw new Error(`Missing/invalid ID [${id}]`);
  }

  try {
    const repo = await db.getRepository(NeocitiesSite);
    const site = await repo.findOneBy({ id });

    return site;
  } catch (err: any) {
    logger?.error({ err, data: { id }}, "failed to retrieve neocities site");
    throw err;
  }
}
