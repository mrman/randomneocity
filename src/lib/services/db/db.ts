import "reflect-metadata";

import * as fs from "node:fs";
import * as path from "node:path";
import { fileURLToPath } from "node:url";

import { Logger } from "pino";
import { DataSource } from "typeorm";
import ndjson from "ndjson";

import { env as SvelteEnv } from "$env/dynamic/private";

import { models } from "$lib/services/db/models"
import { migrations } from "$lib/services/db/migrations"
import { createNeocitiesSite } from "$lib/services/db"

// Global entity manager
let DB: EntityManager | null  = null;

interface GetDBArgs {
  env?: Record<string, string>;
  logger?: Logger;
}

const DEFAULT_TYPEORM_LOGGER = "simple-console";

const DEFAULT_DB_STATEMENT_CACHE_SIZE = 100;
const DEFAULT_DB_MAX_QUERY_EXEC_TIME_MS = 1000;

/**
 * Get the database instance
 *
 * @param {GetDBArgs} [args]
 * @param {Record<string, string>} [args.env]
 * @param {Logger} [args.logger]
 */
export async function getDB(args: GetDBArgs): Promise<Pool> {
  const logger = args?.logger;
  const env = args?.env ?? SvelteEnv;

  // Return the pool if it already exists
  if (DB) { return DB; }

  // Determine database URL
  const dbURL = env.DB_URL;
  if (!dbURL.startsWith("sqlite://")) {
    logger?.warn({ dbURL }, "invalid/unexpected SQLite DB path, expecting scheme 'sqlite://'");
  }

  // Determine statement cache size (only necessary better-sqlite3)
  let statementCacheSize = parseInt(env.DB_STATEMENT_CACHE_SIZE, 10);
  if (Number.isNaN(statementCacheSize)) { statementCacheSize =  DEFAULT_DB_STATEMENT_CACHE_SIZE; }
  logger?.debug(`using statement cache size [${statementCacheSize}]`);

  // Determine statement cache size (only necessary better-sqlite3)
  let maxQueryExecTimeMs = parseInt(env.DB_MAX_QUERY_EXEC_TIME_MS, 10);
  if (Number.isNaN(maxQueryExecTimeMs)) { maxQueryExecTimeMs = DEFAULT_DB_MAX_QUERY_EXEC_TIME_MS; }
  maxQueryExecTimeMs = Math.max(0, maxQueryExecTimeMs);
  logger?.debug(`using maximum query exec time [${maxQueryExecTimeMs}ms]`);

  // Base directory that contains original *and* compiled code
  logger?.debug({ dbURL }, "connecting to database");
  const dbServiceDir = path.dirname(fileURLToPath(import.meta.url));
  const projectBaseDir = path.join(dbServiceDir, "../../../");
  logger?.debug({ projectBaseDir, dbServiceDir }, "detected base directories");

  // Determine entities path
  const entitiesPath = path.join(projectBaseDir, "dist/lib/services/db/models");
  logger?.debug({ entitiesPath }, "using edities");

  // Build the Database
  DB = new DataSource({
    type: "sqlite",
    database: dbURL,

    statementCacheSize,
    maxQueryExecutionTime: maxQueryExecTimeMs,

    logging: env.DB_DEBUG === "true",
    logger: DEFAULT_TYPEORM_LOGGER,

    migrations,
    migrationsRun: true,
    entities: models,
  });

  // Initialize the DB
  try {
    logger?.debug("initializing the db...");
    await DB.initialize();
  } catch (err: any) {
    logger?.error({ err }, "failed to initialize the DB");
    throw err;
  }

  // Load newline delimited JSON if provided
  const seedJSONPath = env.DB_SEED_NDJSON_PATH
  if (seedJSONPath) {
    if (!fs.existsSync(seedJSONPath)) {
      logger?.error({ data: { seedJSONPath }}, "missing/invalid seed file");
      throw new Error(`Missing/invalid seed NDJSON file [${seedJSONPath}]`);
    }

    logger?.info({ seedJSONPath }, "loading seed JSON");

    // Create all sites from the seed file
    // TODO: batch/improve this
    const promises = [];
    await new Promise((resolve, reject) => {
      fs.createReadStream(seedJSONPath)
        .pipe(ndjson.parse())
        .on('data', (site) => {
          promises.push(
            createNeocitiesSite({
              db: DB,
              logger,
              site,
              ignoreFailure: true,
            })
          );
        })
        .on('end', () => resolve())
        .on('error', reject);
    });
    await Promise.all(promises);
  }

  return DB;
};
