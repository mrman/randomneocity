import { MigrationInterface, QueryRunner } from "typeorm"

export class createNeocitiesSitesTable1662456511784 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
CREATE TABLE neocities_sites (
  id INTEGER PRIMARY KEY AUTOINCREMENT,                    -- app-internal contiguous ID for neocities site
  site_id TEXT NOT NULL,                                   -- ID of the site on neocities (ex. site.neocities.com)
  inserted_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP -- When the site was inserted into the DB
);
`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE neocities_sites`);
  }

}
