import { Logger } from "pino";
import { DataSource } from "typeorm";

import { NeocitiesSite } from "$lib/services/db/models";

interface GetNeocitiesSitesTotalArgs {
  db: DataSource;
  logger?: Logger;
}

/**
 * Get the total number of geocities sites
 *
 * @param {GetNeocitiesSitesTotalArgs} args
 * @param {DataSource} args.db
 * @param {Logger} args.logger
 * @returns {Promise<number>} A Promise that resolves to the number of geocities
 */
export async function getNeocitiesSitesTotal(args: GetNeocitiesSitesTotalArgs): Promise<integer> {
  const { db, logger } = args;

  try {
    const repo = await db.getRepository(NeocitiesSite);
    const count = await repo.count();

    return count;
  } catch (err: any) {
    logger?.error({ err }, "failed to retrieve count of neocities sites");
    throw err;
  }
}
