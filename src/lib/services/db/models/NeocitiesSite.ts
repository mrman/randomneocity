import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "neocities_sites" })
export class NeocitiesSite {
  @PrimaryGeneratedColumn()
  id?: number;

  // String that represents the username of the neocities site
  // as in <site>.neocities.com
  @Column({ type: "text", name: "site_id" })
  siteID?: string;

  // @Column({ type: String })
  // description: string;

  @Column({ type: "datetime", name: "inserted_at", nullable: true })
  insertedAt?: Date;

  constructor(opts?: Partial<NeocitiesSite>) {
    this.insertedAt = opts?.insertedAt ?? new Date();

    if (opts) {
      if (opts.id) { this.id = opts.id; }
      if (opts.siteID) { this.siteID = opts.siteID; }
    }
  }

  public toPOJO(model: this) {
    return {
      id: this.id,
      siteID: this.siteID,
    };
  }
}
