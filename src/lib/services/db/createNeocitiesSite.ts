import { Logger } from "pino";
import { DataSource } from "typeorm";

import { NeocitiesSite } from "$lib/services/db/models";

interface CreateNeocitiesSiteArgs {
  db: DataSource;
  logger?: Logger;

  ignoreFailure?: boolean;
  site: Partial<NeocitiesSite>;
}

/**
 * Create a NeocitiesSite in the database
 *
 * @param {CreateNeocitiesSiteArgs} args
 * @param {NeocitiesSite} args.site
 * @param {Logger} [args.logger]
 * @returns {Promise<NeocitiesSite | null>} A Promise that resolves to the created NeocitiesSite
 */
export async function createNeocitiesSite(args: CreateNeocitiesSiteArgs): Promise<NeocitiesSite | null> {
  const { db, logger, site, ignoreFailure } = args;

  if (!site) {
    logger?.error({ data: { site }}, "missing/invalid site site");
    throw new Error(`Missing/invalsite Site [${site}]`);
  }

  try {
    const repo = await db.getRepository(NeocitiesSite);
    const created = await repo.save(new NeocitiesSite(site));
    logger?.debug({ created }, "successfully created neocities site");

    return created;
  } catch (err: any) {
    logger?.error({ err, data: { site }}, "failed to insert neocities site");
    if (ignoreFailure) { return null; }
    throw err;
  }
}
