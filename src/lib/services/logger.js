/* global process */
import { default as getPinoLogger } from "pino";

const LOG_LEVEL = process.env['LOG_LEVEL'] ?? "debug";

export const LOGGER = getPinoLogger({
  name: "randomneocity",
  level: LOG_LEVEL,
});

export const getLogger = async () => LOGGER;

export default getLogger;
