import ndjson from "ndjson";

// Arguments for running script that scrapes latest entries
interface ScrapeLatestEntriesArgs {
  env?: Record<string, string>;
}

const OLDEST_START_URL = "https://neocities.org/browse?sort_by=oldest&tag="
const NEWEST_START_URL = "https://neocities.org/browse?sort_by=newest&tag="

/**
 * Scrape neocities from the listing site
 */
export async function scrapeNeocities(): Promise<void> {
  // Basically....
  // [...document.querySelectorAll(".title > a")].map(v => ({site_id: v.getAttribute('href').replace(".neocities.org","").replace("https://","")}))
  // NOTE: some links will not include 'neocities.net'
  throw new Error("NOT IMPLEMENTED");
}

/**
 * Script that scrapes the latest entries from geocities
 *
 * @param {ScrapeLatestEntriesArgs} [args]
 * @returns {Promise<void>}
 */
export async function main(args?: ScrapeLatestEntriesArgs): Promise<void> {
  // Whether we should do it from the oldest entry first
  const fromOldest = process.env.FROM_OLDEST === "true";
  if (fromOldest) { process.stderr.write("[info] starting from oldest\n"); }

  // Whether we should do it from the newest entry first
  const fromNewest = process.env.FROM_NEWEST === "true";
  if (fromNewest) { process.stderr.write("[info] starting from newest\n"); }

  const scrapePageLIMIT = process.env.SCRAPE_PAGE_LIMIT;
  const scrapePageDelayMS = process.env.SCRAPE_PAGE_DELAY_MS;

    await scrapeNeocities({ limit, fromOldest, fromNewest });

}

// Run main
main();
