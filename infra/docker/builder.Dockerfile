FROM node:17.4.0-alpine

RUN apk add make just git sqlite python3 make g++
RUN npm install -g pnpm

COPY . /app

WORKDIR /app

RUN just install build
