# Infrastructure

While normally the code contained in this folder would be in it's own `-infra` repo (as dictated by the [MakeInfra pattern][makeinfra]), this project is small enough to have infrastructure [IaC][wiki-iac] contained within it.

[makeinfra]: https://vadosware.io/post/using-makefiles-and-envsubst-as-an-alternative-to-helm-and-ksonnet/
[wiki-iac]: https://en.wikipedia.org/wiki/Infrastructure_as_code

## Pulumi

Static infrastructure (ex. DNS records, etc) are

## Kubernetes

More dynamic infrastructure (ex. the application itself) is deployed via Kubernetes, and deployed with [`kustomize`][kustomize].

Note that when deploying the `Makefile` assumes it is running in a properly permissions [`kubectl`][kubectl] context. Such context is normally managed with [`kubie`][kubie]

[kustomize]: https://github.com/kubernetes-sigs/kustomize
[kubectl]: https://kubernetes.io/docs/reference/kubectl/kubectl/
[kubie]: https://github.com/sbstp/kubie/
