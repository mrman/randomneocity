import * as pulumi from "@pulumi/pulumi";

// Setup stack and config
const stack = pulumi.getStack();
const config = new pulumi.Config();

// Server that hosts the actual app (not on AWS)
const rawServerIps: Array<string> = config.requireObject("server-ips");
if (rawServerIps && !Array.isArray(rawServerIps)) { throw new Error(`rawServerIps is not a valid array: ${rawServerIps}`); }
export const serverIps: Array<string> = rawServerIps;

// Base domain
export const baseDomain = config.require("base-domain");

// Prefix for use when creating resources
export const resourcePrefix =  [ baseDomain, stack ].join("-");
