# RandomNeo.City Pulumi configuration #

# Configuration password #

As secrets are protected by `git-crypt` there is no password for unlocking config. Set `PULUMI_CONFIG_PASSPHRASE` to an empty value before running `pulumi up` to remember the password.

# AWS #

`pulumi` user is the IAM user used to change infrastructure.
