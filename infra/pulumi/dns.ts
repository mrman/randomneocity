import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";

import { baseDomain, resourcePrefix, serverIps } from "./common";

///////////////////
// Configuration //
///////////////////

const stack = pulumi.getStack();
const config = new pulumi.Config();

const tld = stack === "production" ? `${baseDomain}` : `${stack}.${baseDomain}`;
const dnsMainZoneID = config.get<string>("dns-main-zone-id");

//////////
// Zone //
//////////

// domain.tld
const mainZone =  new aws.route53.Zone(
  `${baseDomain}-zone`,
  {
    name: baseDomain,
    comment: `${baseDomain}`,
  },
  { id: dnsMainZoneID },
);

export const mainZoneID = mainZone.zoneId;

let zone = mainZone; // this may be changed if not in production

// If we're *not* in production, then we must link the subdomain to the main zone
// so that the subdomain zone's nameservers are also searched
if (stack !== "production") {
  // <stack>.domain.tld
  const stackZone =  new aws.route53.Zone(
    `${tld}-zone`,
    {
      name: tld,
      comment: `${tld}`,
    },
  );

  // Create a record in the production zone to point at staging zone
  new aws.route53.Record(
    tld,
    {
      zoneId: mainZone.zoneId,
      name: tld,
      type: "NS",
      ttl: 30,
      records: stackZone.nameServers,
    },
  );

  // Change the zone for the rest of the work to the stack-specific zone
  zone = stackZone;
}

// NOTE: This zoneID may be the main zone *or* the stack-specific zone
export const zoneID = zone.zoneId;

///////////////
// Top Level //
///////////////

// External-facing top level domain alias "domain.tld" -> "production.domain.tld"
const geoRecords: aws.route53.Record[] =  [];

const dnsRoutingRules: { [key: string]: string[] } = config.requireObject("dns-routing-rules");
if (typeof dnsRoutingRules !== "object" || Object.keys(dnsRoutingRules).some(k => !Array.isArray(dnsRoutingRules[k]))) {
  throw new Error("dns-routing-rules must be an object with values of arrays");
}

// <stack>.domain.tld -> landing & others
const apexRecord =  new aws.route53.Record(
  `${resourcePrefix}-apex`,
  {
    name: tld,
    type: "A",
    zoneId: zone.zoneId,
    setIdentifier: `${resourcePrefix}-geo-default`,
    ttl: 3600,
    geolocationRoutingPolicies: [
      {continent: "", country: "*", subdivision: "*" },
    ],
    records: serverIps,
  }
);

export const apexFQDN = apexRecord.fqdn;

/////////
// WWW //
/////////

// <www>.domain.tld
const wwwRecord =  new aws.route53.Record(
  `${resourcePrefix}-www`,
  {
    name: `www.${tld}`,
    type: "CNAME",
    allowOverwrite: true,
    ttl: 3600,
    zoneId: zone.zoneId,
    records: [ apexRecord.fqdn ],
  },
);

export const wwwFQDN = wwwRecord.fqdn;

/////////////////
// DNS Routing //
/////////////////

// CASE: DNS Georouting rules are specified
const sharedConfig = {
  name: stack === "production" ? `${baseDomain}` : `${stack}.${baseDomain}`,
  type: "A",
  zoneId: zone.zoneId,
  allowOverwrite: true,
  ttl: 3600,
  tags: { environment: stack },
};

// For every country, make the geo-routed apex record if one was cited
for (const [continent, ips] of Object.entries(dnsRoutingRules)) {
  // Ensure IPs are valid
  if (!ips || !Array.isArray(dnsRoutingRules[continent])) {
    throw new Error(`Missing/Invalid list of IPs specified for continent [${continent}]`);
  }

  geoRecords.push(new aws.route53.Record(
    `${baseDomain}-apex-geo-${continent}`,
    {
      ...sharedConfig,
      setIdentifier: `${resourcePrefix}-geo-${continent}`,
      geolocationRoutingPolicies: [ { continent, country: "*", subdivision: "*" } ],
      records: ips,
    }
  ));
}

export const geoRoutedApexRecords = geoRecords;
