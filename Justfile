# Binaries
docker := env_var_or_default("DOCKER", "docker")
pnpm := env_var_or_default("PNPM", "pnpm")
node := env_var_or_default("NODE", "node")
entr := env_var_or_default("ENTR", "entr")
just := env_var_or_default("JUST", "just")
typeorm := env_var_or_default("TYPEORM", "npx typeorm")
git := env_var_or_default("GIT", "git")
make := env_var_or_default("MAKE", "make")

version := `node -e 'console.log(require("./package.json").version);'`
sha := `git rev-parse --short HEAD`

default:
    @{{just_executable()}} --list

#############
# Utilities #
#############

print-version:
    @echo -n "{{version}}"

print-sha:
    @echo -n "{{sha}}"

changelog_file_path := env_var_or_default("CHANGELOG_FILE_PATH", "CHANGELOG")

changelog:
  {{git}} cliff --unreleased --tag={{version}} --prepend={{changelog_file_path}}

#########
# Build #
#########

install:
    @{{pnpm}} install

lint:
    @{{pnpm}} run lint
    @{{pnpm}} run check

fmt:
    @{{pnpm}} run format

build:
    @{{pnpm}} run build

dev:
    @{{pnpm}} run dev

serve: build
    @{{node}} server.js

serve-watch: build
  find src/ \
    -name "*.ts" \
    -or -name "*.svelte" \
    -or -name "*.js" \
    | {{entr}} -rc {{just}} serve

preview:
    @{{pnpm}} run preview

deploy-app:
    {{make}} -C infra/kubernetes app-deployment

deploy:
    {{make}} -C infra/kubernetes

#######################
# Database management #
#######################

migration_name := env_var_or_default("NAME", "new-migration")
db_migrations_dir := env_var_or_default("DB_MIGRATIONS_DIR", `realpath src/lib/services/db/migrations`)

create-db-migration:
  {{typeorm}} migration:create {{db_migrations_dir}}/{{migration_name}}

########
# Test #
########

test:
    @{{pnpm}} run test

test-unit:
    @{{pnpm}} run test:unit

test-int:
    @{{pnpm}} run test:int

test-e2e:
    @{{pnpm}} run test:e2e

#############
# Packaging #
#############

app_dockerfile_path := env_var_or_default("APP_DOCKERFILE_PATH", "./infra/docker/Dockerfile")

docker_image_registry := env_var_or_default("DOCKER_IMAGE_REGISTRY", "registry.gitlab.com")
project_registry := env_var_or_default("DOCKER_IMAGE_REGISTRY", "registry.gitlab.com/mrman/randomneocity")

docker_password_path := env_var_or_default("DOCKER_PASSWORD_PATH", "./secrets/docker/password.secret")
docker_user := env_var_or_default("DOCKER_USER_PATH", "fill this in")

docker-login:
    @cat {{docker_password_path}} | {{docker}} login {{docker_image_registry}} -u {{docker_user}} --password-stdin
    @cp $DOCKER_CONFIG/config.json $DOCKER_CONFIG/.dockerconfigjson

docker_app_image_name := env_var_or_default("DOCKER_IMAGE_NAME", "app")
docker_app_image_version := env_var_or_default("DOCKER_IMAGE_VERSION", version)
docker_app_image_full := project_registry + "/" + docker_app_image_name + ":" + docker_app_image_version
docker_app_image_full_sha := docker_app_image_full + "-" + sha

build-app-image:
    @{{docker}} build -f {{app_dockerfile_path}} -t {{docker_app_image_full_sha}} .

push-app-image:
    @{{docker}} push {{docker_app_image_full_sha}}

release-app-image: build-app-image
    @{{docker}} tag {{docker_app_image_full_sha}} {{docker_app_image_full}}
    @{{docker}} push {{docker_app_image_full}}

builder_dockerfile_path := env_var_or_default("BUILDER_DOCKERFILE_PATH", "./infra/docker/builder.Dockerfile")
docker_builder_image_name := env_var_or_default("DOCKER_IMAGE_NAME", "builder")
docker_builder_image_version := env_var_or_default("DOCKER_IMAGE_VERSION", version)
docker_builder_image_full := project_registry + "/" + docker_builder_image_name + ":" + docker_builder_image_version

build-builder-image:
    {{docker}} build -f {{builder_dockerfile_path}} -t {{docker_builder_image_full}} .

push-builder-image:
    {{docker}} push {{docker_builder_image_full}}

###########
# Scripts #
###########

script-find-new: build
    @{{node}} scripts/
