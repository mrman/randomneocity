# [randomneo.city][site]

![neocities cat with question marks in the background](./static/images/neocities-neko-with-questionmarks.png)

This repository holds the code for [randomneo.city][site] a blatant rip off of [randomgeo.city][randomgeo], but with 100% more [Neocities][neocities].

**NOTE: This project is NOT an official project related to [Neocities.org][neocities] -- logos/images of the Neocities cat belong to Neocities.org and related entities**

## Deploying

### Docker image

The pre-built docker image for can be found in the [container registry attached to this repo][repo-registry]. As the images are public, you can run them on your cluster them straight from there (you may be subject to pull limits).

### Environment

The following environment variables are required:

| ENV                         | Default                    | Example                      | Description                                                            |
|-----------------------------|----------------------------|------------------------------|------------------------------------------------------------------------|
| `DB_URL`                    | `sqlite://path/to/file.db` | `sqlite:///tmp/neocities.db` | The URL of the SQLite database                                         |
| `DB_STATEMENT_CACHE_SIZE`   | `100`                      | `50`                         | Statement cache size to use with [`better-sqlite3`][better-sqlite3]    |
| `DB_MAX_QUERY_EXEC_TIME_MS` | `1000`                     | `500`                        | Maximum amount of time until a query is logged                         |
| `DB_DEBUG`                  | N/A                        | `true`                       | Whether to print debug logging                                         |
| `DB_SEED_NDJSON_PATH`       | N/A                        | `/path/to/sites.ndjson.json` | [Newline Delimited JSON][ndjson] file which contains sites to pre-load |

[better-sqlite3]: https://github.com/WiseLibs/better-sqlite3

## Seeding Data

If you'd like to seed data (required to get a page to show up), use `sqlite3`:

```console
sqlite3 local.db "INSERT INTO neocities_sites (site_id) VALUES ('joe');"
```

Given a newline delimited JSON file at `/path/to/sites.ndjson.json` that looks like this:

```json
{"site_id": "joe"}
{"site_id": "habbo"}
```

You can set the app to load the files (and use all entries) by setting `DB_SEED_NDJSON_PATH`.

[ndjson]: https://dataprotocols.org/ndjson/

## Local development

### Dependencies

- [`just`][just] (an alternative to [GNU Make][gnu-make])
  - `make` is also in use for infrastructure stuff, as I'm really "just" trying out `just` on this repo.
- [PNPM][pnpm]
- [Docker][docker]
- [SvelteKit][sveltekit]

### Unlock secrets with `git-crypt`

Start by unlocking the repo secrets:

```console
git-crypt unlock
```

### Set up ENV

Local `.envrc` ([direnv][direnv]):

```bash
git config --local core.hooksPath $PWD/.githooks

## Project
export DOCKER_CONFIG=$(realpath secrets/docker)
export DOCKER_PASSWORD_PATH=$(realpath secrets/docker/password.secret)
export DOCKER_USER_PATH=$(cat secrets/docker/user.secret)

## Infra - Pulumi
export AWS_ACCESS_KEY_ID=$(cat secrets/pulumi/aws-access-key-id.secret)
export AWS_SECRET_ACCESS_KEY=$(cat secrets/pulumi/aws-secret-access-key.secret)
export PULUMI_CONFIG_PASSPHRASE=$(cat secrets/pulumi/password-encryption.secret)

## Environment
export NODE_ENV=development
export ENVIRONMENT=development

## Server
export PORT=3001

## Database
export DB_URL=$(realpath .)/local.db
export DB_SEED_NDJSON_PATH=$(realpath seed-latest.ndjson.json)
```

[direnv]: https://direnv.net

### Building the project locally

To build the project, run the `build` target:

```console
just build
```

## Running the server

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```console
just dev
```

You can run a more production-like server:

```console
just serve
just serve-watch # reload-on-change mode
```

### Run the tests

To run all local tests:

```console
just test
```

To run just a specific kind of tests, you may use `test:<type of test>`:

```console
just test-unit # run the unit tests
just test-int  # run the integration tests
just test-e2e  # run the e2e tests
```

Note that E2E tests require installation of `docker`.

## Packaging

### Building the docker container

To build the docker container for manually

```console
just docker-image
```

## TODO

- [ ] Automate scraper (currently 500 entries in the preseed JSON, but ideally it could be the complete listing, or a ready-to-use SQLite DB)

[just]: https://github.com/casey/just
[gnu-make]: https://www.gnu.org/software/make
[pnpm]: https://pnpm.io
[docker]: https://docs.docker.com/
[site]: https://randomneo.city
[pg]: https://postgresql.org
[randomgeo]: https://randomgeo.city
[neocities]: https://neocities.org
[repo]: https://gitlab.com/mrman/randomneocity
[repo-registry]: https://gitlab.com/mrman/randomneocity/container_registry
[sveltekit]: https://kit.svelte.dev
