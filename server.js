/* global process */
import express from "express";

import { LOGGER } from "./src/lib/services/logger.js";
import { DEFAULT_PORT } from "./src/lib/constants.js";
import { buildApp } from "./app.js";

const PORT = parseInt(process.env['PORT'] ?? `${DEFAULT_PORT}`, 10);

const APP_CONFIG = {
  postgres: {
    url: process.env['DB_URL'],
  },
};

async function main() {
  // Build app
  const app = await buildApp(APP_CONFIG, { registerProcessExceptions: true });

  // Start listening
  app.listen(PORT, () => {
    LOGGER.info(`listening on port [${PORT}]`);
  });

}

main();
