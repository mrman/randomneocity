/* global process */
import * as fs from "fs";
import express from "express";

import * as Sentry from "@sentry/node";
import "@sentry/tracing"; // patches global hub

import { handler as svelte } from "./build/handler.js";

// import { createMigrator } from "./src/lib/services/db/migrator.js";
import { LOGGER } from "./src/lib/services/logger.js";

/**
 * Build the application
 *
 * @returns {express.Application}
 */
export async function buildApp(config, opts) {
  const logger = opts?.logger || LOGGER;

  // Build the application
  const app = express();

  // Setup Sentry (middleware *must* be first)
  Sentry.init({
    dsn: process.env.PUBLIC_SENTRY_DSN,
    tracesSampleRate: 1.0,
  });
  app.use(Sentry.Handlers.requestHandler());

  // Svelte
  app.use(svelte);

  // Sentry Error handler must be before error middleware, or after everything else
  app.use(Sentry.Handlers.errorHandler());

  if (opts?.registerProcessExceptions) {
    // Attempt to catch uncaught exceptions
    process.on("uncaughtException", (err, source) => {
      console.log("UNCAUGHT EXCEPTION", err);
      console.log("SOURCE", source);
      fs.writeSync(process.stderr.fd, err, source);
    });

    // Attempt to catch uncaught exceptions
    process.on("unhandledRejection", (reason, promise) => {
      console.log("UNHANDLED EXCEPTION", reason);
      console.log("Promise", promise);
    });

    // Attempt to catch uncaught exceptions
    process.on("unhandledRejection", (warning) => {
      console.log("NODE PROCESS WARNING", warning);
    });
  }

  return app;
}
